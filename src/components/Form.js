import React, {Component} from 'react';


class Form extends Component {
    render(){
        return(
            <div className='form-container'>
                <h1>Web Weather App</h1>

                <form onSubmit={this.props.getWeather}>
                    <input type="text" name='city' placeholder='City...'/>
                    <input type="text" name='country' placeholder='Country...'/>
                    <button className='btn'>Get Weather</button>
                </form>

            </div>
        );
    }
}

export default Form;
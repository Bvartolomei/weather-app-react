import React, {Component} from 'react';

class Weather extends Component {
    render(){
        return(
            <div className='weather-container'>
                {this.props.city && this.props.country && <p>City : {this.props.city} {this.props.country}</p>}
                {this.props.wind && <p>Wind: {this.props.wind} km/h</p>}
                {this.props.temperature && <p>Temperature : {this.props.temperature} &#8451;</p>}
                {this.props.humidity && <p>Humidity: {this.props.humidity}</p>}
                {this.props.description && <p>Conditions: {this.props.description}</p>}
                {this.props.error && <p>{this.props.error}</p>}
            </div>
        );
    }
}

export default Weather;
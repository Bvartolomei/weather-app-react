import React, { Component } from 'react';

import Form from './components/Form';
import Weather from './components/Weather';
import './App.scss';

const API_KEY = 'a52c6b46f3ab971bce7fb84a09e9c57d';

class App extends Component{

  state = {
    city: '',
    country: '',
    wind: '',
    temperature: '',
    humidity: '',
    description: '',
    error: ''
  }

  getWeather = async (event) => {

    event.preventDefault();

    const city = event.target.elements.city.value;
    const country = event.target.elements.country.value;
    const call_api = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`);
    const data = await call_api.json();

    if (city && country){
      this.setState({

        city: data.name,
        country: data.sys.country,
        wind: data.wind.speed,
        temperature: data.main.temp,
        humidity: data.main.humidity,
        description: data.weather[0].description,
        error: ''
  
      });
    } else {
      this.setState({
        city: '',
        country: '',
        wind: '',
        temperature: '',
        humidity: '',
        description: '',
        error: 'Please type a city and a country.'
      });
  }
}

  render(){
    return(
      <div className='wrapper'>
        <Form getWeather={this.getWeather}/>
        <Weather 
            city={this.state.city}
            country={this.state.country}
            wind={this.state.wind}
            temperature={this.state.temperature}
            humidity={this.state.humidity}
            description={this.state.description}
            error={this.state.error}
        />
      </div>
    );
  }
}




export default App;